**Mosaic Aquatic Artworks**

Mosaicist offers specialized assistance for the installation of mosaic mediums on any kind of application, surface or technique, specifically swimming pools, spas, showers, fountains, and many other water facades. We are among the few companies that are able to offer a complete mosaic project, including design, manufacture, preparation, and installation. Commissions of old world designs to contemporary aquatic motifs are presented before execution. Our mosaic masterpieces will keep their lush appearance and color to last a lifetime.
[https://mosaicist.com/aquatic-artworks/](https://mosaicist.com/aquatic-artworks/)

